﻿using UnityEngine;
using System.Collections;

public class ObjectHandler : MonoBehaviour {

    /*Class overview
     * Determine amount of objects to light up by difficulty
     * determine which item to light up via random gen
     * light up item
     * store in array which item was lit up
     * wait for a bit
     * repeat
     * when player answers, checks players answer in answer array against spawn array. 
     * if sequence matches 100%,  answer = correct 
     */

    /*
     * every time player clicks{+1 to answers[]. if answers[i] == randomSequence[i], +1 to correctAnswers.
     * if correctanswers >= randomSequence[], questionCorrect = true}
     */

    private int diffMult = 2;
    private int sequenceSize = 4;
    public int difficulty = 0;

    private int chosenDot;

    private int[] randomSequence;
    private int[] playerSequence;

    public GameObject[] dots = new GameObject[5];

	void Start () {
        StartSequence();
	}
	
	void Update () {
	
	}

    //checks how many items need to be sequenced, then runs the sequence, putting each item into a list.
    public void StartSequence()
    {
        //how big a sequence is
        diffMult *= difficulty;
        sequenceSize += diffMult;
        //sets the lengths of int[] randomSequence
        randomSequence = new int[sequenceSize];
        
        //chooses object i, then fills randSequence in for which item was chosen, then sets chosen object.chosen to true for a time, then sets it back to false.
        for (int i = 0; i < sequenceSize; i++)
        {
            var randDot = (int)Random.Range(0, 5);
            randomSequence[i] = randDot;

            StartCoroutine(Sequence(randDot));
        }
    }

    IEnumerator Sequence(int randDot)
    {
            dots[randDot].GetComponent<ExampleDotScript>().chosen = true;
            yield return new WaitForSeconds(1.5f);
            dots[randDot].GetComponent<ExampleDotScript>().chosen = false;
            yield return new WaitForSeconds(1f);
            Debug.Log("dot chosen");
    }


}
