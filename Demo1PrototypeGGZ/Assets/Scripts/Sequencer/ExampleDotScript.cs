﻿using UnityEngine;
using System.Collections;

public class ExampleDotScript : MonoBehaviour {

    public bool chosen = false;

    public Texture2D[] textures;
    private Texture tex1;
    private Texture tex2;

    private SpriteRenderer rend;
    private ParticleSystem parts;

	void Start () {
        tex1 = textures[0];
        tex2 = textures[1];
        rend = this.GetComponent<SpriteRenderer>();
        parts = this.GetComponent<ParticleSystem>();

        rend.material.mainTexture = textures[0];
        parts.enableEmission = false;
	}
	
	void Update () {
        if (chosen)
        {
            rend.material.mainTexture = tex1;
            parts.enableEmission = true;
        }
        else if (!chosen)
        {
            rend.material.mainTexture = tex2;
            parts.enableEmission = false;
        }
	}
}
