﻿using UnityEngine;
using System.Collections;

public class ObjectHandlerScript : MonoBehaviour {


    /* class overview:
     * Find child SpawnArea
     * Check size/position of child SpawnArea
     *generate random int for object A 
     *generate random int for object B
     *generate random int for object c
     *spawn object A randomly within spawnArea untill there are [random int] objects A
     *scale object A randomly between 0.5 and 0.2
     *repeat for objects B, C
     *if a random int == another random int, rerol random int.
     */
    public int minSpawns = 5;
    public int maxSpawns = 25;

    public float minScale = 0.1f;
    public float maxScale = 0.3f;
    private float randScale;

    public GameObject objectA;
    public GameObject objectB;
    public GameObject objectC;

    private Vector3 randPos;
    private GameObject spawnArea;

    private Vector3 spawnTopLeft;
    private Vector3 spawnTopRight;
    private Vector3 spawnBottomLeft;
    private Vector3 spawnBottomRight;

    public int randAmountA;
    public int randAmountB;
    public int randAmountC;

    public bool reset = true;

	void Start () {
        spawnArea = this.transform.Find("SpawnArea").gameObject;
        DetermineSpawnArea();
        /*
        Debug.Log("topLeft = " + spawnTopLeft);
        Debug.Log("topRight = " + spawnTopRight);
        Debug.Log("botLeft = " + spawnBottomLeft);
        Debug.Log("botRight = " + spawnBottomRight);
        */
        Spawn();
	}

    void Update()
    {
        DetermineSpawnArea();
    }
	
    //generates a random amount of game objects in random places within the SpawnArea object
    private void Spawn()
    {
        //makes all the objects selfdestruct
        reset = false;

        //determines how many of each object will get spawned
        randAmountA = Random.Range(minSpawns, maxSpawns + 1);
        randAmountB = Random.Range(minSpawns, maxSpawns + 1);
        randAmountC = Random.Range(minSpawns, maxSpawns + 1);

        //reruns the random number generation if 2 or more numbers are the same.
        if (randAmountA == randAmountB | randAmountB == randAmountC | randAmountA == randAmountC)
        {
            Spawn();
        }
        //unique random numbers initiates spawning at a random position within bounds and with a random scale.
        else
        {
            for (int i = 0; i < randAmountA; i++)
            {
                var instance = Instantiate(objectA, new Vector3(Random.Range(spawnTopLeft.x, spawnBottomRight.x), Random.Range(spawnTopLeft.y, spawnBottomRight.y), -0.01f), Quaternion.identity) as GameObject;
                randScale = Random.Range(minScale, maxScale);
                instance.transform.localScale = new Vector3(randScale, randScale, 1);
            }

            for (int i = 0; i < randAmountB; i++)
            {
                var instance = Instantiate(objectB, new Vector3(Random.Range(spawnTopLeft.x, spawnBottomRight.x), Random.Range(spawnTopLeft.y, spawnBottomRight.y), -0.01f), Quaternion.identity) as GameObject;
                randScale = Random.Range(minScale, maxScale);
                instance.transform.localScale = new Vector3(randScale, randScale, 1);            
            }

            for (int i = 0; i < randAmountC; i++)
            {
                var instance = Instantiate(objectC, new Vector3(Random.Range(spawnTopLeft.x, spawnBottomRight.x), Random.Range(spawnTopLeft.y, spawnBottomRight.y), -0.01f), Quaternion.identity) as GameObject;
                randScale = Random.Range(minScale, maxScale);
                instance.transform.localScale = new Vector3(randScale, randScale, 1);
            }
        }
        Debug.Log("Amount of A: " + randAmountA);
        Debug.Log("Amount of B: " + randAmountB);
        Debug.Log("Amount of C: " + randAmountC);
    }

    //Checks the current position of the 4 corners of SpawnArea object, even after being scaled to new proportions.
    private void DetermineSpawnArea()
    {
     Bounds bounds = spawnArea.GetComponent<SpriteRenderer>().sprite.bounds;
     Vector3 center = bounds.center + (spawnArea.transform.position - this.transform.position);
     Vector3 extents = bounds.extents;
  
     spawnTopLeft     = new Vector3(center.x - extents.x, center.y + extents.y, -0.01f) *0.9f;  // Front top left corner
     spawnTopRight    = new Vector3(center.x + extents.x, center.y + extents.y, -0.01f) *0.9f;  // Front top right corner
     spawnBottomLeft  = new Vector3(center.x - extents.x, center.y - extents.y, -0.01f) *0.9f;  // Front bottom left corner
     spawnBottomRight = new Vector3(center.x + extents.x, center.y - extents.y, -0.01f) *0.9f;  // Front bottom right corner
         
     spawnTopLeft     = transform.TransformPoint(spawnTopLeft);
     spawnTopRight    = transform.TransformPoint(spawnTopRight);
     spawnBottomLeft  = transform.TransformPoint(spawnBottomLeft);
     spawnBottomRight = transform.TransformPoint(spawnBottomRight); 

    }
}
