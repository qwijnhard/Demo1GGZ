﻿using UnityEngine;
using System.Collections;

public class SelfDestructScript : MonoBehaviour {

    ObjectHandlerScript objectHandler;

    void Start()
    {
        GameObject theObjectHandler = GameObject.Find("ObjectHandler");
        objectHandler = theObjectHandler.GetComponent<ObjectHandlerScript>();
    }

    void Update()
    {
        if (objectHandler.reset)
        {
            SelfDestruct();
        }
    }

	void SelfDestruct()
    {
        Destroy(this.gameObject);
	}
}
