﻿using UnityEngine;
using System.Collections;

public class AreaSpawnScaleScript : MonoBehaviour {

    GameObject theBackGround;
    Vector3 scale;

	void Start () {
        theBackGround = GameObject.Find("Background");
	}
	
	void Update () 
    {
        this.transform.localScale = theBackGround.transform.localScale;
	}
}
