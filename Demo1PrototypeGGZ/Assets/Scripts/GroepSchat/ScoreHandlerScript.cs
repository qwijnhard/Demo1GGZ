﻿using UnityEngine;
using System.Collections;

public class ScoreHandlerScript : MonoBehaviour {
    /*Class overview:
     *
     *Start timer as soon as the level begins
     *Read amounts of  Object A, B, C from ObjectHandlerScript
     *Compare Object counts to see which is higher
     *Tag the highest counted object as the answer
     */

    private float elapsedTime;
    private bool timerRunning;

    private int amountA;
    private int amountB;
    private int amountC;

    public int[] answers = new int[3];

    //for now the item names are hardcoded but there will need to be a class that this var will read from to get the proper name from.
    public string item = ""; 

	void Start () {
 //Starts time as soon as the object is instantiated.
        StartTimer();
	}

	void Update () {
        if (timerRunning)
        {
            elapsedTime += Time.deltaTime;
            //Debug.Log((int)elapsedTime);
        }
/* used to test StopTimer before i had buttons
        if (elapsedTime >= 5)
        {
            StopTimer();
        }
*/
	}

    //Sets elapsedTime to 0 and then starts the timer
    public void StartTimer()
    {
        elapsedTime = 0;
        timerRunning = true;
    }

    //Stops the timer and reads how many of each object there are
    public void StopTimer()
    {
        timerRunning = false;

        GameObject theObjectHandler = GameObject.Find("ObjectHandler");
        ObjectHandlerScript objectHandler = theObjectHandler.GetComponent<ObjectHandlerScript>();
        amountA = objectHandler.randAmountA;
        amountB = objectHandler.randAmountB;
        amountC = objectHandler.randAmountC;

        answers[0] = amountA;
        answers[1] = amountB;
        answers[2] = amountC;

    }

    //Checks to see which object has the most entities and returns which has the most
    public void DetermineCorrect()
    {
         int maxIndex=-1;
         int maxValue=0;
         for (int i = 0; i < answers.Length; i++)
         {
             if (answers[i] > maxValue)
             {
                 maxValue = answers[i];
                 maxIndex = i;
             }
         }

         if (maxIndex == 0)
         {
             item = "Object A";
         }
         else if (maxIndex == 1)
         {
             item = "Object B";
         }
         else if (maxIndex == 2)
         {
             item = "Object C";
         }

         Debug.Log(item + " has the most instances with " + answers[maxIndex] + "instances.");
         Debug.Log("It took " + (int)elapsedTime + " seconds to get your answer!");
    }
}
