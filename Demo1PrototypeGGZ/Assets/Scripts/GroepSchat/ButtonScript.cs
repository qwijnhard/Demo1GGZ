﻿using UnityEngine;
using System.Collections;

public class ButtonScript : MonoBehaviour {

    /*
     *Read player's input
     *If the answer matched the correct asnwer, stop the timer, give the text "Correct! there were [correct answer] of the [theme] type! you did it in [Time]!" <-- in dutch, though.
     *Else, "wrong, there were [correct answer] of a different [theme] type. it took you [time]"
     *store time in data file
     *Read all stored times, calculate average time 
     *if most recent time is lower than the average, give the text "your average time is [average time]! ?"
     *if most recent time is lower than the average, give the text "your average time is [average time]! nice!"
     */

    public GameObject answer;
    public string buttonAnswer;

    ScoreHandlerScript scoreHandler;

	void Start () {/*
        //find the scorehandler to read from
        GameObject theScoreHandler = GameObject.Find("ObjectHandler");
        scoreHandler = gameObject.GetComponent<ScoreHandlerScript>();*/

        //sets button's answer to the name of the linked gameObject
        buttonAnswer = answer.transform.name;
	}
	
	void Update () {

	}


    public void CheckAnswer(GameObject scoreHandlerObj)
    {
        if (scoreHandler == null)
        {
            Debug.Log("no scoreHandler found");
            scoreHandler = scoreHandlerObj.GetComponent<ScoreHandlerScript>();

        }
            scoreHandler.StopTimer();
            scoreHandler.DetermineCorrect();

        
        if (this.buttonAnswer == scoreHandler.item)
        {
            Debug.Log("you are correct, sir!");
        }
        else
        {
            Debug.Log("you are not correct, sir!");
        }
    }

}
