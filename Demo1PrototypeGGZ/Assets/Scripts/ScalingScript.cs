﻿using UnityEngine;
using System.Collections;

public class ScalingScript : MonoBehaviour {

	//This script needs to be added to anything that needs to fill the screen so it scales with the device's screen size
    SpriteRenderer sr;

	void Start () 
    {
        sr = GetComponent<SpriteRenderer>();
	}

    void Update()
    {
        float worldScreenHeight = Camera.main.orthographicSize * 2;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        transform.localScale = new Vector3(
            worldScreenWidth / sr.sprite.bounds.size.x,
            worldScreenHeight / sr.sprite.bounds.size.y, 1);
    }
	
}
